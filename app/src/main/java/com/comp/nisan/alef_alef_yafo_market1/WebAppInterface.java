package com.comp.nisan.alef_alef_yafo_market1;

/**
 * Created by doron on 31/10/2018.
 */


import android.content.Context;
import android.webkit.JavascriptInterface;

/**
 * Created by doron on 02/10/2017.
 */

public class WebAppInterface {
    Context mContext;

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */

    @JavascriptInterface
    public int order_confirmed(String op_code1) {
        CGlobals1.send_message("close_activity1");
        //Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        //CGlobals1.send_message("js1");

        return 1;
    }

}
