package com.comp.nisan.alef_alef_yafo_market1;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by doron on 03/11/2018.
 */


/**
 * Created by doron on 03/11/2018.
 */
public class HttpRequestTask extends AsyncTask<String, Void, String> {

    public String str_result="";

    private Context context;
    private String url;
    public String op_code1="";

    public HttpRequestTask(String url, Context context) {


        this.url = url;
        this.context = context;
    }



    @Override
    protected String doInBackground(String... params) {
        URL url2 = null;
        HttpURLConnection conn = null;
        byte[] postDataBytes =null;
        /*if (postParams!=null) {
            postDataBytes = generatePostData();
        }*/

        try
        {
            //url = new URL("http://taxiapp.co.il.viphostingns.biz/checkimei.asp");
            url2 = new URL(url);
            conn = (HttpURLConnection) url2.openConnection();
            conn.setConnectTimeout(10000*30);
            conn.setReadTimeout(10000*30);
            //conn.setRequestMethod("POST");
            //conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            if (postDataBytes!=null) {
                conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                conn.setDoOutput(true);
                //conn.getOutputStream()
                //conn.
                OutputStream os1=conn.getOutputStream();
                os1.write(postDataBytes);
            }

            //.write(postDataBytes);
        }
        catch(  java.net.SocketTimeoutException ex1) {
            String str1 = ex1.getMessage().toString();
            return "err1=socket_time_out1";
        }
        catch (Exception e)
        {
            String str1=e.getMessage().toString();
                /*if (e.getMessage().toString().contains("java.net.SocketTimeoutException")==true)
                {
                    return "err1=socket_time_out1";
                }*/

            e.printStackTrace();
            return "err1=socket1";
        }

        /* Retrieve response */
        Reader in = null;
        String response = "";
        try
        {
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            response = "";
            for (int c = in.read(); c != -1; c = in.read())
            {
                response += (char) c;
            }
        }
        catch(  java.net.SocketTimeoutException ex1)
        {
            //java.net.Socketre
            //CGlobals1.send_log_message("timeout-err2"+op_code_finidhed1);
            String str1=ex1.getMessage().toString();
            return "err1=socket_time_out1";
        }
        catch (Exception e)
        {
            //CGlobals1.send_log_message("err1"+op_code_finidhed1);

            e.printStackTrace();
            return "err1=socket2";
        }
        //resultString = response;

            /*if (true) {
                return "";
            }*/
        return response;
        //return downloadBitmap("http://nisancomp.co.il/media/1230/logo.jpg");
//return null;
        //return downloadBitmap(params[1]);
    }

    @Override
    protected void onPostExecute(String txt1) {

        str_result=txt1;

        if (op_code1.compareTo("get_marketer_name1")==0) {
            CGlobals1.send_message1("marketer_name1");
        }

        if (op_code1.compareTo("activate_device_id1")==0) {
            CGlobals1.send_message1("check_device_activate1");
        }

        if (op_code1.compareTo("register_device_id1")==0) {
            CGlobals1.send_message1("check_register_device_id1");
        }

        //if (op_code1=="activate_device_id1")
    }

}