package com.comp.nisan.alef_alef_yafo_market1;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {

    ImageDownloaderTask logo_downloader=null;
    HttpRequestTask http_request_task_marketer_name1=null;
    HttpRequestTask http_request_activate_device_id1=null;
    HttpRequestTask http_request_register_device_id1=null;
    public static final Handler handler1 = new Handler() {
        public void handleMessage(Message msg) {

            Bundle bundle1 = msg.getData();
            String op_code1 = bundle1.getCharSequence("op_code1").toString();



            if (op_code1.compareTo("check_register_device_id1")==0)
            {
                if (((MainActivity) CGlobals1.enter_activity1).http_request_register_device_id1.str_result.compareTo("no_exist")==0)
                {
                    CGlobals1.dialoglayout.findViewById(R.id.note_wrong1).setVisibility(View.VISIBLE);
                    //note_wrong1
                    //((MainActivity) CGlobals1.enter_activity1).ShowAlert1(null);

                }
                else
                {
                    try
                    {
                        CGlobals1.customer_num1 = ((MainActivity) CGlobals1.enter_activity1).http_request_register_device_id1.str_result.split("=")[1];
                    }
                    catch(Exception ex1)
                    {
                        CGlobals1.dialoglayout.findViewById(R.id.note_wrong1).setVisibility(View.VISIBLE);
                        return;
                    };
                    CGlobals1.alertDialog.cancel();
                    Intent myIntent = new Intent(((MainActivity) CGlobals1.enter_activity1), OrderActivity1.class);
                    myIntent.putExtra("key", "111"); //Optional parameters
                    ((MainActivity) CGlobals1.enter_activity1).startActivity(myIntent);
                }
            }


            if (op_code1.compareTo("check_device_activate1")==0)
            {
                if (((MainActivity) CGlobals1.enter_activity1).http_request_activate_device_id1.str_result.compareTo("no_exist")==0)
                {
                    ((MainActivity) CGlobals1.enter_activity1).ShowAlert1(null);
                }
                else
                {
                    //doron
                    //CGlobals1.alertDialog.cancel();
                    CGlobals1.customer_num1=((MainActivity) CGlobals1.enter_activity1).http_request_activate_device_id1.str_result.split("=")[1];
                    Intent myIntent = new Intent(((MainActivity) CGlobals1.enter_activity1), OrderActivity1.class);
                    myIntent.putExtra("key", "111"); //Optional parameters
                    ((MainActivity) CGlobals1.enter_activity1).startActivity(myIntent);

                }
            }
            if (op_code1.compareTo("marketer_name1")==0)
            {
                String res_str1=((MainActivity) CGlobals1.enter_activity1).http_request_task_marketer_name1.str_result;
                String supp_name1=res_str1.split("#")[0];
                String supp_phone1=res_str1.split("#")[1];
                ((TextView)CGlobals1.view_bar1.findViewById(R.id.action_bar_text1)).setText(supp_name1);
                //CGlobals1.enter_activity1.findViewById(R.id.textview_resolution1).setVisibility(View.INVISIBLE);



                TextView text_view_phone1=CGlobals1.enter_activity1.findViewById(R.id.textview_phonenumber1);
                text_view_phone1.setText(supp_phone1);


                TextView btn1=CGlobals1.enter_activity1.findViewById(R.id.btn_enter1);


                String txt_enter1=" כניסה להזמנות של "+"\r\n";
                txt_enter1+=supp_name1;
                btn1.setText(txt_enter1);
            }

            if (op_code1.compareTo("update_layout1")==0)
            {
                TextView btn1=CGlobals1.enter_activity1.findViewById(R.id.btn_enter1);
                AbsoluteLayout.LayoutParams lay_prms1=(AbsoluteLayout.LayoutParams)btn1.getLayoutParams();
                lay_prms1.x=CGlobals1.width_enter_layout1/2-lay_prms1.width/2;
                lay_prms1.y=CGlobals1.height_enter_layout1/2-lay_prms1.height/2;
                btn1.setLayoutParams(lay_prms1);


                String txt_enter1=" כניסה להזמנות של ";
                //txt_enter1+="הזמ. אסף ומוטי";
                btn1.setText(txt_enter1);
                ImageView img_view1=CGlobals1.enter_activity1.findViewById(R.id.customer_logo_img1);
                lay_prms1=(AbsoluteLayout.LayoutParams)img_view1.getLayoutParams();
                lay_prms1.width=CGlobals1.width_enter_layout1/5;
                lay_prms1.height=CGlobals1.width_enter_layout1/5;
                lay_prms1.x=CGlobals1.width_enter_layout1-lay_prms1.width-CGlobals1.width_enter_layout1/20;
                lay_prms1.y=CGlobals1.width_enter_layout1/20;
                img_view1.setLayoutParams(lay_prms1);
                img_view1.setVisibility(View.VISIBLE);


                img_view1=CGlobals1.enter_activity1.findViewById(R.id.nisanlogo_img1);
                lay_prms1=(AbsoluteLayout.LayoutParams)img_view1.getLayoutParams();
                lay_prms1.width=CGlobals1.width_enter_layout1/5;
                lay_prms1.height=CGlobals1.width_enter_layout1/5;
                lay_prms1.x=CGlobals1.width_enter_layout1/20;
                lay_prms1.y=CGlobals1.width_enter_layout1/20;
                img_view1.setLayoutParams(lay_prms1);
                img_view1.setVisibility(View.VISIBLE);

                TextView text_view1=CGlobals1.enter_activity1.findViewById(R.id.by_nisan_comp_text1);
                lay_prms1=(AbsoluteLayout.LayoutParams)text_view1.getLayoutParams();
                lay_prms1.width=CGlobals1.width_enter_layout1/2;
                lay_prms1.height=70;
                lay_prms1.x=CGlobals1.width_enter_layout1/4;
                lay_prms1.y=CGlobals1.height_enter_layout1-lay_prms1.height-CGlobals1.height_enter_layout1/20;
                text_view1.setLayoutParams(lay_prms1);
                text_view1.setVisibility(View.VISIBLE);
                String txt1="ביצוע ניסן מחשבים בע/\"מ 050-5298492";
                text_view1.setText(txt1);

                TextView text_view_phone1=CGlobals1.enter_activity1.findViewById(R.id.textview_phonenumber1);
                lay_prms1=(AbsoluteLayout.LayoutParams)text_view1.getLayoutParams();
                lay_prms1.width=CGlobals1.width_enter_layout1/2;
                lay_prms1.height=70;
                lay_prms1.x=CGlobals1.width_enter_layout1/4;
                lay_prms1.y=CGlobals1.height_enter_layout1/20;
                text_view_phone1.setLayoutParams(lay_prms1);
                text_view_phone1.setVisibility(View.VISIBLE);
                String txt2="1111111111";
                text_view_phone1.setText(txt2);


                ((MainActivity)(CGlobals1.enter_activity1)).logo_downloader.execute();
                ((MainActivity)(CGlobals1.enter_activity1)).http_request_task_marketer_name1.execute();

                //CGlobals1.enter_activity1.findViewById(R.id.textview_phonenumber1).setVisibility(View.INVISIBLE);
                CGlobals1.enter_activity1.findViewById(R.id.textview_resolution1).setVisibility(View.INVISIBLE);
                //by_nisan_comp_text1
                //customer_logo_img1
                //((OrderActivity1)OrderActivity1.this).finish();
            }
        }
    };

    public static final int REQUEST_READ_PHONE_STATE = 0;
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                }
                break;

            default:
                break;
        }
    }




    public static final String main_data[] = {
            "data1", "is_primary", "data3", "data2", "data1", "is_primary", "photo_uri", "mimetype"
    };

    public String getDeviceIMEI() {
        String deviceUniqueIdentifier = null;
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            try
            {
                deviceUniqueIdentifier = tm.getDeviceId();

            }
            catch(Exception ex2)
            {

            };
        }
        if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
            deviceUniqueIdentifier = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return deviceUniqueIdentifier;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        //setContentView(R.layout.main2);
        setContentView(R.layout.enter1);
        CGlobals1.enter_activity1=this;

        //Toast.makeText(this, "ok1", Toast.LENGTH_LONG).show();
int d1=1;
        //d1/=0;

        File file1=CGlobals1.get_cache_file1();
        String path1=file1.getPath()+"/"+CGlobals1.nisan_app_folder+"/log1.txt";

        CGlobals1.CreateFile2(path1);
        CGlobals1.logger1("ok1");
        CGlobals1.imei1=getDeviceIMEI();
        logo_downloader=new ImageDownloaderTask((ImageView) findViewById(R.id.customer_logo_img1),"http://nisancomp.co.il/media/"+CGlobals1.MARKETER+"/logo.jpg",this);
        http_request_task_marketer_name1=new HttpRequestTask("https://nisancomp.co.il/get_supplier_name1.php?v2=yes&m="+CGlobals1.MARKETER,this);
        http_request_task_marketer_name1.op_code1="get_marketer_name1";
        CGlobals1.logger1("ok2");


        //http_request_activate_device_id1=new HttpRequestTask("http://nisancomp.co.il/activate_device_id1.php?op_code1=check_device_id1&m=1230&ms="+CGlobals1.imei1,this);
        //http_request_activate_device_id1.op_code1="activate_device_id1";





        //http_request_activate_device_id1.execute();

        /*Window window = this.getWindow();





        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.setStatusBarColor(Color.argb(255,255,255,255));*/

        ActionBar bar = this.getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        CGlobals1.logger1("ok3");
        CGlobals1.view_bar1=getLayoutInflater().inflate(R.layout.actionbar1, null);
        CGlobals1.view_bar1.setMinimumHeight(200);;
        bar.setCustomView(CGlobals1.view_bar1,
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.CENTER
                )
        );
        CGlobals1.logger1("ok4");
        //((TextView)CGlobals1.view_bar1.findViewById(R.id.action_bar_text1)).setText("הזמ. אסף ומוטי - 3");

        //ActivityUtils.setActionBarColor(this, R.color.green_00c1c1);

        //ActionBar actionBar = getActionBar();    // Or getSupportActionBar() if using appCompat
        //int red = Color.RED
        //setActionbarTextColor2(bar,Color.parseColor("#2CA343") );

        final AbsoluteLayout layout = (AbsoluteLayout) findViewById(R.id.enter_layout1);
        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //vto.removeOnGlobalLayoutListener();
                layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                if (CGlobals1.finish_enter_layout1==0) {
                    CGlobals1.finish_enter_layout1 = 1;
                    CGlobals1.width_enter_layout1 = layout.getMeasuredWidth();
                    CGlobals1.height_enter_layout1 = layout.getMeasuredHeight();

                    CGlobals1.send_message1("update_layout1");
                }

            }
        });

        CGlobals1.logger1("ok5");

/*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<SubscriptionInfo> subscription = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
            for (int i = 0; i < subscription.size(); i++) {
                SubscriptionInfo info = subscription.get(i);
                String str1=info.getNumber();
                //String str2=info.getCarrierName();
                String str3=info.getCountryIso();
                String str4=String.valueOf(info.getMnc());
                int d1=1;
                 d1++;
                //Log.d(TAG, "number " + info.getNumber());
                //Log.d(TAG, "network name : " + info.getCarrierName());
                //Log.d(TAG, "country iso " + info.getCountryIso());
            }
        }
*/



        try
        {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
            } else {
                //TODO
            }

        }
        catch(Exception ex1)
        {
            CGlobals1.logger1("err_log1");
        };

        CGlobals1.logger1("ok6");


        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        TextView tv1=(TextView)findViewById(R.id.textview_resolution1);
        tv1.setText( "{" + width + "," + height + "}");
        //return "{" + width + "," + height + "}";
        CGlobals1.logger1("ok7");

        TextView btn_enter1=(TextView)findViewById(R.id.btn_enter1);

        btn_enter1.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View view) {
                                                   http_request_activate_device_id1=new HttpRequestTask("http://nisancomp.co.il/activate_device_id1.php?op_code1=check_device_id1&m="+CGlobals1.MARKETER+"&ms="+CGlobals1.imei1,CGlobals1.enter_activity1);
                                                   http_request_activate_device_id1.op_code1="activate_device_id1";

                                                   http_request_activate_device_id1.execute();
                                                   /*Intent myIntent = new Intent(MainActivity.this, OrderActivity1.class);
                                                   myIntent.putExtra("key", "111"); //Optional parameters
                                                   MainActivity.this.startActivity(myIntent);*/
                                               }
                                           }
        );



        CGlobals1.logger1("ok8");




        TelephonyManager tMgr = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);




        String mPhoneNumber="empty1";
        try
        {

            mPhoneNumber = tMgr.getLine1Number();
            /*mPhoneNumber = tMgr.getSubscriberId();
            mPhoneNumber = tMgr.getDeviceId();*/

        }
        catch (Exception ex1)
        {
            try
            {
                mPhoneNumber=tMgr.getSimSerialNumber();
            }
            catch(Exception ex2)
            {
                mPhoneNumber="no_phone1";
            };


        };
        CGlobals1.logger1("ok9");

        TextView text_view1=(TextView)findViewById(R.id.textview_phonenumber1);
        text_view1.setText(mPhoneNumber);
        CGlobals1.logger1("ok10");

        //textview_phonenumber1

    }

    private void setActionbarTextColor2(ActionBar actBar, int color) {

        String title = actBar.getTitle().toString();
        Spannable spannablerTitle = new SpannableString(title);
        spannablerTitle.setSpan(new ForegroundColorSpan(color), 0, spannablerTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        actBar.setTitle(spannablerTitle);

    }


    public static void setActionbarTextColor(Activity activity, int color) {
        Field mActionViewField;
        try {
            mActionViewField = activity.getActionBar().getClass()
                    .getDeclaredField("mActionView");
            mActionViewField.setAccessible(true);
            Object mActionViewObj = mActionViewField.get(activity
                    .getActionBar());

            Field mTitleViewField = mActionViewObj.getClass().getDeclaredField(
                    "mTitleView");
            mTitleViewField.setAccessible(true);
            Object mTitleViewObj = mTitleViewField.get(mActionViewObj);

            TextView mActionBarTitle = (TextView) mTitleViewObj;
            mActionBarTitle.setTextColor(color);
            // Log.i("field", mActionViewObj.getClass().getName());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }


    public static String ShowAlert1(View.OnClickListener on_click_listener1) {


        AlertDialog.Builder builder = new AlertDialog.Builder(CGlobals1.enter_activity1);
        //builder.setTitle(title_str);
        //builder.setMessage("Enter password");
        final FrameLayout frameView = new FrameLayout(CGlobals1.enter_activity1);
        builder.setView(frameView);

        CGlobals1.alertDialog = builder.create();
        CGlobals1.alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = CGlobals1.alertDialog.getLayoutInflater();
        CGlobals1.dialoglayout = inflater.inflate(R.layout.register1, frameView);
        Button btn_obj1=CGlobals1.dialoglayout.findViewById(R.id.btn_ok2);

        btn_obj1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edittext_obj1=CGlobals1.dialoglayout.findViewById(R.id.code_txt1);
                String str1="http://nisancomp.co.il/activate_device_id1.php?op_code1=register_device_id1&m="+CGlobals1.MARKETER+"&ms="+CGlobals1.imei1+"&secure_code1="+edittext_obj1.getText();
                ((MainActivity) CGlobals1.enter_activity1).http_request_register_device_id1=new HttpRequestTask(str1,CGlobals1.enter_activity1);
                ((MainActivity) CGlobals1.enter_activity1).http_request_register_device_id1.op_code1="register_device_id1";
                ((MainActivity) CGlobals1.enter_activity1).http_request_register_device_id1.execute();
                //CGlobals1.alertDialog.cancel();
            }
        });
        //CViewManager.set_on_click_view(dialoglayout, on_click_listener1);
        WindowManager.LayoutParams wmlp1 = CGlobals1.alertDialog.getWindow().getAttributes();
        wmlp1.x=30;
        wmlp1.y=30;
        wmlp1.width=300;
        wmlp1.height=300;


        //alertDialog.getWindow().setAttributes(wmlp1);
        CGlobals1.alertDialog.getWindow().setLayout(wmlp1.width, wmlp1.height);
        CGlobals1.alertDialog.getWindow().getAttributes().x=30;
        CGlobals1.alertDialog.getWindow().getAttributes().y=30;
        //alertDialog
        //alertDialog.getWindow().setGravity(Gravity.FILL);
        //alertDialog.getWindow().setw
        CGlobals1.alertDialog.show();
        /*CDict_prms1 dict_obj1=new CDict_prms1();
        dict_obj1.set_value("alertDialog", alertDialog);
        dict_obj1.set_value("frameView", frameView);
        dict_obj1.set_value("dialoglayout", dialoglayout);
        CGlobals1.dict_prms_obj1.set_value("dialoglayout", dialoglayout);*/
        return "";
    }
}
