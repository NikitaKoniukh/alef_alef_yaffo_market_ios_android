package com.comp.nisan.alef_alef_yafo_market1;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;

public class OrderActivity1 extends AppCompatActivity {



    public static final Handler handler1 = new Handler() {
        public void handleMessage(Message msg) {

            Bundle bundle1 = msg.getData();
            String op_code1 = bundle1.getCharSequence("op_code1").toString();

            if (op_code1.compareTo("close_activity1")==0)
            {
                OrderActivity1 order_Activity1=(OrderActivity1)CGlobals1.activity1;
                order_Activity1.finish();
                //((OrderActivity1)OrderActivity1.this).finish();
            }
        }
    };




    WebAppInterface webappintr1=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order1);


        ActionBar bar = this.getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        bar.hide();
        /*View view_bar1=getLayoutInflater().inflate(R.layout.actionbar1, null);

        bar.setCustomView(view_bar1,
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.CENTER
                )
        );
        //((TextView)view_bar1.findViewById(R.id.action_bar_text1)).setText("הזמ. אסף ומוטי - 3");
        ((TextView)view_bar1.findViewById(R.id.action_bar_text1)).setText(((MainActivity) CGlobals1.enter_activity1).http_request_task_marketer_name1.str_result);*/
        CGlobals1.activity1=this;
        WebView wv = (WebView) this.findViewById(R.id.WebView1);

        //wv.setWebChromeClient(new NiasnWebChromeClient());
        wv.setWebChromeClient(new WebChromeClient());

        //NisanWebViewClient wvc1=new NisanWebViewClient();

        WebViewClient wvc1=new WebViewClient();
        //CGlobals1.log1("show_screen1-1-3");

        wv.clearCache(true);
        //CGlobals1.log1("show_screen1-1-4");




        wv.clearHistory();
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.getSettings().setDomStorageEnabled(true);
        //CGlobals1.log1("show_screen1-1-5");
        //((MainActivity)this).webappintr1=new WebAppInterface(CGlobals1.activity1);
        //wv.addJavascriptInterface(((MainActivity)this).webappintr1, "Android");




        //CGlobals1.log1(wv.getSettings().getUserAgentString());


        Display display =this.getWindowManager().getDefaultDisplay();
        int width2 = display.getWidth();
        int height2 = display.getHeight();


                /*AbsoluteLayout.LayoutParams params = ((AbsoluteLayout.LayoutParams) wv.getLayoutParams());
                params.x = 0;
                params.y = 0;
                params.height=height2;
                params.width=width2;
                wv.setLayoutParams(params);*/

                /*PackageManager pm = CGlobals1.activity1.getPackageManager();
                PackageInfo pi = pm.getPackageInfo("com.google.android.webview", 0);*/
        //Log.d(TAG, "version name: " + pi.versionName);
        //Log.d(TAG, "version code: " + pi.versionCode);


        wv.setWebViewClient(new NisanWebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here

            }
        });


                /*WebViewClient wvc2=new WebViewClient(){

                };*/


        wv.setWebViewClient(wvc1);

        ((OrderActivity1) this).webappintr1 = new WebAppInterface(this);
        wv.addJavascriptInterface(((OrderActivity1) this).webappintr1, "Android");

        String postData = "submit=yes&enter_mobile1=1&username="+CGlobals1.marketer_user1+"&password="+CGlobals1.marketer_pass1+"&customer_num1="+CGlobals1.customer_num1;
        //webview.postUrl(url",EncodingUtils.getBytes(postData, "BASE64"));
        //wv.loadUrl(CGlobals1.file_ext1+CGlobals1.cache_path1+"/main_html1/1.html");
        //wv.loadUrl()
        byte [] post_data=postData.getBytes();
        wv.postUrl("http://nisancomp.co.il/m_login.php",post_data);//EncodingUtils.getBytes(postData, "BASE64"));

        //ShowAlert1(null);
    }


    public static String ShowAlert1(View.OnClickListener on_click_listener1) {


        AlertDialog.Builder builder = new AlertDialog.Builder(CGlobals1.activity1);
        //builder.setTitle(title_str);
        //builder.setMessage("Enter password");
        final FrameLayout frameView = new FrameLayout(CGlobals1.activity1);
        builder.setView(frameView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.alert1, frameView);
        Button btn_obj1=dialoglayout.findViewById(R.id.btn_ok1);
        btn_obj1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        //CViewManager.set_on_click_view(dialoglayout, on_click_listener1);
        WindowManager.LayoutParams wmlp1 = alertDialog.getWindow().getAttributes();
        wmlp1.x=30;
        wmlp1.y=30;
        wmlp1.width=300;
        wmlp1.height=300;


        //alertDialog.getWindow().setAttributes(wmlp1);
        alertDialog.getWindow().setLayout(wmlp1.width, wmlp1.height);
        alertDialog.getWindow().getAttributes().x=30;
        alertDialog.getWindow().getAttributes().y=30;
        //alertDialog
        //alertDialog.getWindow().setGravity(Gravity.FILL);
        //alertDialog.getWindow().setw
        alertDialog.show();
        /*CDict_prms1 dict_obj1=new CDict_prms1();
        dict_obj1.set_value("alertDialog", alertDialog);
        dict_obj1.set_value("frameView", frameView);
        dict_obj1.set_value("dialoglayout", dialoglayout);
        CGlobals1.dict_prms_obj1.set_value("dialoglayout", dialoglayout);*/
        return "";
    }

}
